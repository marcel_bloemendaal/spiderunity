﻿public enum TouchDownResponse {
	Ignore,
	ClaimTouch,
	InitiateDragAndDrop
}
