﻿using UnityEngine;

public interface ITouchResponder {
	TouchDownResponse OnTouchDown(Vector2 position);
	void OnTouchMove(Vector2 position);
	void OnTouchUp(Vector2 position);
}
