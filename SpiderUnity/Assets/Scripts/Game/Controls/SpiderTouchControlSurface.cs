﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class SpiderTouchControlSurface : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	public static SpiderTouchControlSurface Current { get; protected set; }

	[SerializeField]
	protected SpiderThreadShooter spiderThreadShooter;

	protected HashSet<ITouchResponder> touchResponders = new HashSet<ITouchResponder>();
	protected ITouchResponder activeTouchResponder;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Touch handling
	//---------------------------------------------------

	public void OnPointerDown(PointerEventData data) {
		if (data.pointerId == 0 || data.pointerId == -1) {
			foreach (ITouchResponder responder in this.touchResponders) {
				switch(responder.OnTouchDown(data.position)) {
					case TouchDownResponse.ClaimTouch:
						this.activeTouchResponder = responder;
						break;
						
					case TouchDownResponse.InitiateDragAndDrop:
						break;
						
					case TouchDownResponse.Ignore:
						break;
				}
			}
		}
	}

	public void OnDrag(PointerEventData data) {
		if (data.pointerId == 0 || data.pointerId == -1) {
			if (this.activeTouchResponder != null) {
				this.activeTouchResponder.OnTouchMove(data.position);
			}
		}
	}

	public void OnPointerUp(PointerEventData data) {
		if (data.pointerId == 0 || data.pointerId == -1) {
			if (this.activeTouchResponder != null) {
				this.activeTouchResponder.OnTouchUp(data.position);
			} else {
				Vector3 position = Camera.main.ScreenToWorldPoint(data.position);
				position.z = 0.0f;
				this.spiderThreadShooter.ShootThread(position);
			}
			this.activeTouchResponder = null;
		}
	}

	//---------------------------------------------------
	//  TouchResponders
	//---------------------------------------------------

	public void AddTouchResponder(ITouchResponder responder) {
		this.touchResponders.Add(responder);
	}

	public void RemoveTouchResponder(ITouchResponder responder) {
		this.touchResponders.Remove(responder);
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		Current = this;
	}
}
