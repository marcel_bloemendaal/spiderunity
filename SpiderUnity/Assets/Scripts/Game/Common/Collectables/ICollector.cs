﻿

public interface ICollector {
	void CollectHealth(int amount);
	void CollectFullHealth();
	void CollectFly();
}
