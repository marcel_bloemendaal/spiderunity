﻿using UnityEngine;

public abstract class Collectable : MonoBehaviour, IThreadResponder, ICheckpointEntity {
	protected bool collected = false;
	protected bool wasCollectedOnLastCheckpoint = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public ThreadResponse RespondToThread() {
		return ThreadResponse.GetCollected;
	}

	public void GetCollected(ICollector collector) {
		this.ApplyCollectionEffect(collector);
		this.gameObject.SetActive(false);
		this.collected = true;
	}

	//--------------------------------------------------
	//  IChecipointEntity
	//--------------------------------------------------

	public void ResetToOriginalState() {
		this.setCollected(false);
		this.wasCollectedOnLastCheckpoint = false;
	}

	public void SaveCheckpoint(Firefly firefly) {
		this.wasCollectedOnLastCheckpoint = this.collected;
	}

	public void ResetToCheckpoint() {
		this.setCollected(this.wasCollectedOnLastCheckpoint);
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Start() {
		Environment.Current.RegisterCheckpointEntity(this);
	}

	protected void OnDestroy() {
		Environment.Current.UnregisterCheckpointEntity(this);
	}

	//--------------------------------------------------
	//  Other
	//--------------------------------------------------

	protected void OnTriggerEnter2D(Collider2D other) {
		if (!this.collected) {
			ICollector collector = other.GetComponent<ICollector>();
			if (collector != null) {
				this.GetCollected(collector);
			}
		}
	}

	protected void setCollected(bool collected) {
		this.collected = collected;
		this.gameObject.SetActive(!collected);
	}

	abstract protected void ApplyCollectionEffect(ICollector collector);
}
