﻿using System;
public class FlyCollectable : Collectable {


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	override protected void ApplyCollectionEffect(ICollector collector) {
		collector.CollectFly();
	}
}
