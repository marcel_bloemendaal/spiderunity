﻿
public class HealthCollectable : Collectable {

	public int AmountOfHealth = 1;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	override protected void ApplyCollectionEffect(ICollector collector) {
		collector.CollectHealth(this.AmountOfHealth);
	}
}
