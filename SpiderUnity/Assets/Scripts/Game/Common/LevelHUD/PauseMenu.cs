﻿using UnityEngine;

public class PauseMenu : MonoBehaviour {

	public delegate void DismissHandler();

	public event DismissHandler Dismiss;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  User interaction
	//--------------------------------------------------

	public void OnContinueButtonClick() {
		this.dismiss();
	}

	public void OnRestartLevelButtonClick() {
		this.dismiss();
		Environment.Current.Restart();
	}

	public void OnQuitButtonClick() {
		this.dismiss();
		Environment.Current.Quit();
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void dismiss() {
		Environment.Current.ReleasePause();
		this.gameObject.SetActive(false);
		this.Dismiss?.Invoke();
	}
}
