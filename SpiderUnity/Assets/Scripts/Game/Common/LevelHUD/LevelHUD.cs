﻿using UnityEngine;
using UnityEngine.UI;

public class LevelHUD : MonoBehaviour {

	[SerializeField] protected Button pauseButton;
	[SerializeField] protected PauseMenu pauseMenu;

	public static LevelHUD Current { get; protected set; }


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------


	//--------------------------------------------------
	//  User interaction
	//--------------------------------------------------

	public void OnPauseButtonClick() {
		if (!this.pauseMenu.gameObject.activeSelf) {
			this.pauseButton.interactable = false;
			Environment.Current.RetainPause();
			this.pauseMenu.gameObject.SetActive(true);
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		Current = this;
		this.pauseMenu.Dismiss += this.onPauseMenuDismissed;
	}

	protected void OnDestroy() {
		if (this.pauseButton != null) {
			this.pauseMenu.Dismiss -= this.onPauseMenuDismissed;
		}
	}

	//--------------------------------------------------
	//  Events
	//--------------------------------------------------

	protected void onPauseMenuDismissed() {
		this.pauseButton.interactable = true;
	}
}
