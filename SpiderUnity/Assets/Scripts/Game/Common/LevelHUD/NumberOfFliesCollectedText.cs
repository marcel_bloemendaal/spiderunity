﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class NumberOfFliesCollectedText : MonoBehaviour {
	protected Text text;
	protected bool started = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.text = this.GetComponent<Text>();
	}

	protected void Start() {
		this.updateText();
		Environment.Current.Spider.NumberOfFliesCollectedChanged += this.onNumberOfFliesCollectedChanged;
		this.started = true;
	}

	protected void OnDestroy() {
		if (this.started) {
			Environment.Current.Spider.NumberOfFliesCollectedChanged -= this.onNumberOfFliesCollectedChanged;
		}
	}

	//--------------------------------------------------
	//  Updating
	//--------------------------------------------------

	protected void updateText() {
		this.text.text = Environment.Current.Spider.NumberOfFliesCollected.ToString();
	}

	//--------------------------------------------------
	//  Events
	//--------------------------------------------------

	protected void onNumberOfFliesCollectedChanged(int numberOfFliesCollected) {
		this.updateText();
	}
}
