﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Spiderweb : MonoBehaviour {

	protected Animator animator;
	protected bool triggered = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Pulbic methods
	//
	//------------------------------------------------------------------------------------------------------

	public void PlayVictoryAnimation() {
		this.animator.Play("Victory");
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.animator = this.GetComponent<Animator>();
	}

	protected void OnTriggerEnter2D(Collider2D collision) {
        if (!this.triggered) {
			Spider spiderController = collision.gameObject.GetComponent<Spider>();
			if (spiderController != null) {
				this.triggered = true;
				Environment.Current.Finish();
			}
		}
	}
}
