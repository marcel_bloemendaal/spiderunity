﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;

public class Environment : MonoBehaviour {

	protected const float RespawnDelay = 3.0f;

	public enum EnvironmentState {
		Active,
		Paused,
		Finishing,
		Finished
	}

	public static Environment Current { get; protected set; }

	public EnvironmentState State { get; protected set; }

	[SerializeField] protected Spider spider;
	[SerializeField] protected Spiderweb spiderweb;
	protected HashSet<ICheckpointEntity> checkpointEntities = new HashSet<ICheckpointEntity>();
	protected int pauseRetainCount;
	protected EnvironmentState stateBeforePaused;


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public Spider Spider {
		get {
			return this.spider;
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Control
	//--------------------------------------------------

	public void RetainPause() {
		if (++this.pauseRetainCount == 1) {
			Time.timeScale = 0.0f;
			this.stateBeforePaused = this.State;
			this.State = EnvironmentState.Paused;
		}
	}

	public void ReleasePause() {
		if (--this.pauseRetainCount == 0) {
			Time.timeScale = 1.0f;
			this.State = this.stateBeforePaused;
		}
	}

	//--------------------------------------------------
	//  Registration
	//--------------------------------------------------

	public void RegisterCheckpointEntity(ICheckpointEntity entity) {
		this.checkpointEntities.Add(entity);
	}

	public void UnregisterCheckpointEntity(ICheckpointEntity entity) {
		this.checkpointEntities.Remove(entity);
	}

	//--------------------------------------------------
	//  Checkpoints
	//--------------------------------------------------

	public void SaveCheckpoint(Firefly firefly) {
		foreach (ICheckpointEntity entity in this.checkpointEntities) {
			entity.SaveCheckpoint(firefly);
		}
	}

	public void Restart() {

		// Copy set as entities could be added / removed during the procedure
		HashSet<ICheckpointEntity> stateEntitiesCopy = new HashSet<ICheckpointEntity>(this.checkpointEntities);
		foreach (ICheckpointEntity entity in stateEntitiesCopy) {
			entity.ResetToOriginalState();
		}
	}

	public void Quit() {
		SceneManager.LoadScene("LevelMenu");
	}

	//--------------------------------------------------
	//  Finishing
	//--------------------------------------------------

	public void Finish() {
		if (this.State == EnvironmentState.Active) {
			this.State = EnvironmentState.Finishing;
			this.Spider.PlayVictoryAnimation(this.spiderweb.transform.position);
			this.spiderweb.PlayVictoryAnimation();
		}
	}

	public void Fail() {
		this.StartCoroutine(resetToCheckpointWithDelay(RespawnDelay));
	}

	//--------------------------------------------------
	//  Util
	//--------------------------------------------------

	public void ExecuteWhenReady(Action action) {

	}

	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void Awake() {
		Environment.Current = this;
		Application.targetFrameRate = 60;
	}

	protected IEnumerator resetToCheckpointWithDelay(float delay) {
		yield return new WaitForSeconds(delay);
		this.resetToCheckpoint();
	}

	//--------------------------------------------------
	//  Checkpoints
	//--------------------------------------------------

	protected void resetToCheckpoint() {

		// Copy set as entities could be added / removed during the procedure
		HashSet<ICheckpointEntity> stateEntitiesCopy = new HashSet<ICheckpointEntity>(this.checkpointEntities);
		foreach (ICheckpointEntity entity in stateEntitiesCopy) {
			entity.ResetToCheckpoint();
		}
	}
}
