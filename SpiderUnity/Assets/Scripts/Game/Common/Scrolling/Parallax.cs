﻿using UnityEngine;

public class Parallax : MonoBehaviour {

	[SerializeField]
	protected Camera targetCamera;
	[SerializeField]
	protected float factor = 0.25f;

	protected float invertedFactor;
	protected Vector3 originalPosition;
	protected Vector3 originalCameraPosition;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void Start() {
		this.invertedFactor = 1.0f - this.factor;
		this.originalPosition = this.transform.position;
		this.originalCameraPosition = this.targetCamera.transform.position;
	}

	protected void Update() {
		Vector3 cameraOffset = this.targetCamera.transform.position - this.originalCameraPosition;
		this.transform.position = this.originalPosition + this.invertedFactor * cameraOffset;
	}
}
