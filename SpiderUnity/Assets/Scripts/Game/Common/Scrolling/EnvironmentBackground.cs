﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class EnvironmentBackground : MonoBehaviour {

	[SerializeField]
	protected Camera targetCamera;
	[SerializeField]
	protected float parallaxFactor = 0.25f;

	protected Vector3 originalCameraPosition;
	protected MeshRenderer meshRenderer;
	protected float width;
	protected float height;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void Start() {
		this.originalCameraPosition = this.targetCamera.transform.position;
		this.meshRenderer = this.GetComponent<MeshRenderer>();

		// Measure view size in units
		this.transform.localScale = this.targetCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0.0f)) - this.targetCamera.ScreenToWorldPoint(Vector3.zero);
		this.width = this.transform.localScale.x;
		this.height = this.transform.localScale.y;

		// Set tiling of material to match screen size
		float textureWidth = this.meshRenderer.material.mainTexture.width;
		float textureHeight = this.meshRenderer.material.mainTexture.height;
		this.meshRenderer.material.mainTextureScale = new Vector2(Screen.width / ((textureWidth / textureHeight) * Screen.height), 1.0f);
	}

	protected void LateUpdate() {
		Vector3 position = this.targetCamera.transform.position;
		position.z = this.transform.position.z;
		this.transform.position = position;
		Vector3 cameraOffset = this.targetCamera.transform.position - this.originalCameraPosition;
		Vector3 offset = this.parallaxFactor * cameraOffset;
		offset.Set((offset.x % this.width) / this.width, (offset.y % this.height) / this.height, 0.0f);
		this.meshRenderer.material.mainTextureOffset = offset;
	}
}
