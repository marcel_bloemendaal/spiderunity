﻿using UnityEngine;
using System.Collections;

public class EyesBlinker : MonoBehaviour {

	public float MinBlinkInterval = 0.1f;
	public float MaxBlinkInterval = 10.0f;
	public float BlinkDuration = 0.1f;

	[SerializeField] protected BlinkEyelid[] eyeLids;


	protected float timeLeft;
	protected bool eyesClosed = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Start() {
		foreach (BlinkEyelid eyelid in this.eyeLids) {
			eyelid.SetClosed(false);
		}
		this.delayNewBlink();
	}

	protected void Update() {
		this.timeLeft -= Time.deltaTime;
		if (this.timeLeft <= 0.0f) {
			if (this.eyesClosed) {
				foreach (BlinkEyelid eyelid in this.eyeLids) {
					eyelid.SetClosed(false);
				}
				this.eyesClosed = false;
				this.delayNewBlink();
			} else {
				foreach (BlinkEyelid eyelid in this.eyeLids) {
					eyelid.SetClosed(true);
				}
				this.eyesClosed = true;
				this.timeLeft = this.BlinkDuration;
			}
		}
	}

	//---------------------------------------------------
	//  Other
	//---------------------------------------------------

	protected void delayNewBlink() {
		this.timeLeft = Random.Range(MinBlinkInterval, MaxBlinkInterval);
	}
}
