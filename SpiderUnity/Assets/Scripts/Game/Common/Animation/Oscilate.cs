﻿using UnityEngine;

public class Oscilate : MonoBehaviour {

	public float CycleDuration = 1.0f;
	public float MaxHorizontalOffset = 0.0f;
	public float MaxVerticalOffset = 0.1f;
	public float NormalizedStartOffset = 0.0f;
	public bool RandomizeStartOffset = false;

	protected Vector3 centerPosition;
	protected Vector3 direction;
	protected float angle;
	protected float angleSpeed;
	protected bool horizontalEnabled;
	protected bool verticalEnabled;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void Awake() {
		this.centerPosition = this.transform.position;
		this.direction = new Vector3(this.MaxHorizontalOffset, this.MaxVerticalOffset, 0.0f);
		this.angle = this.RandomizeStartOffset ? Random.Range(0.0f, Mathf.PI * 2.0f) : this.NormalizedStartOffset * Mathf.PI * 2.0f;
		this.angleSpeed = Mathf.PI * 2.0f / this.CycleDuration;
		this.horizontalEnabled = !Mathf.Approximately(0.0f, this.direction.x);
		this.verticalEnabled = !Mathf.Approximately(0.0f, this.direction.y);
	}

	protected void Update() {
		this.angle = (this.angle + Time.deltaTime * this.angleSpeed) % (Mathf.PI * 2.0f);
		Vector3 position = this.transform.position;
		float factor = Mathf.Sin(this.angle);
		if (this.horizontalEnabled) {
			position.x = this.centerPosition.x + factor * this.direction.x;
		}
		if (this.verticalEnabled) {
			position.y = this.centerPosition.y + factor * this.direction.y;
		}
		this.transform.position = position;
	}
}
