﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class RandomSpriteSequencer : MonoBehaviour {

	[SerializeField] protected List<SpriteSequence> spriteSequences;
	[SerializeField] protected float frameInterval;
	[SerializeField] protected float minInterval;
	[SerializeField] protected float maxInterval;

	protected SpriteRenderer spriteRenderer;
	protected Sprite baseSprite;
	protected Sprite[] currentSequence;
	protected float currentTime;
	protected float currentDelay;


	//------------------------------------------------------------------------------------------------------
	//
	//  MonoBehaviour life cycle
	//
	//------------------------------------------------------------------------------------------------------

	protected void Awake() {
		this.spriteRenderer = this.GetComponent<SpriteRenderer>();
		this.baseSprite = this.spriteRenderer.sprite;
		this.setNextSequence();	
	}

	protected void Update() {
		this.currentTime += Time.deltaTime;
		float sequenceTime = this.currentTime - this.currentDelay;
		if (sequenceTime >= 0.0f) {
			int frameIndex = (int)(sequenceTime / this.frameInterval);
			if (frameIndex < this.currentSequence.Length) {
				this.spriteRenderer.sprite = this.currentSequence[frameIndex];
			} else {
				this.spriteRenderer.sprite = this.baseSprite;
				this.setNextSequence();
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Animation
	//
	//------------------------------------------------------------------------------------------------------

	protected void setNextSequence() {
		this.currentTime = 0.0f;
		this.currentDelay = Random.Range(this.minInterval, this.maxInterval);
		this.currentSequence = this.spriteSequences[Random.Range(0, this.spriteSequences.Count)].Sprites;
	}
}

[System.Serializable]
public class SpriteSequence {
	[SerializeField] protected Sprite[] sprites;

	public Sprite[] Sprites => this.sprites;
}
