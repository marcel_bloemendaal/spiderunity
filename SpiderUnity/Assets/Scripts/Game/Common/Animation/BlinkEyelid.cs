﻿using UnityEngine;

[System.Serializable]
public class BlinkEyelid {

	[SerializeField] protected SpriteRenderer spriteRenderer;
	[SerializeField] protected Sprite openSprite;
	[SerializeField] protected Sprite closedSprite;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void SetClosed(bool closed) {
		this.spriteRenderer.sprite = closed ? this.closedSprite : this.openSprite;
	}
}
