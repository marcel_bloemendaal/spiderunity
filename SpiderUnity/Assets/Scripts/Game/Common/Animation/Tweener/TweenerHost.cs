﻿using UnityEngine;
using System.Collections.Generic;

public class TweenerHost : MonoBehaviour {

	protected static TweenerHost current;

	protected List<ITweener> tweeners = new List<ITweener>();
	protected bool updating = false;
	protected List<ITweener> tweenersRemovedDuringUpdate = new List<ITweener>();


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public static TweenerHost Current {
		get {
			if (current == null) {
				GameObject hostGameObject = new GameObject();
				hostGameObject.AddComponent<TweenerHost>();
			}
			return current;
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void AddTweener(ITweener tweener) {
		this.tweeners.Add(tweener);
	}

	public void RemoveTweener(ITweener tweener)	{
		if (this.updating) {
			this.tweenersRemovedDuringUpdate.Add(tweener);
		} else {
			this.tweeners.Remove(tweener);
		}
	}

	public void StopAllTweeners(Transform target) {
		for (int index = this.tweeners.Count - 1; index >= 0; --index) {
			ITweener tweener = this.tweeners[index];
			if (tweener.Target == target) {
				tweener.Stop();
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		if (current != null && current != this) {
			Destroy(this.gameObject);
		} else {
			current = this;
		}
	}

	protected void Update() {
		this.updating = true;
		foreach (ITweener tweener in this.tweeners) {
			tweener.Update();
		}
		this.updating = false;
		foreach(ITweener tweener in this.tweenersRemovedDuringUpdate) {
			this.tweeners.Remove(tweener);
		}
		this.tweenersRemovedDuringUpdate.Clear();
	}

	protected void OnDestroy() {
		this.tweeners.Clear();
	}
}
