﻿using UnityEngine;
using System;

public class Tweener : ITweener {

	public delegate float TweenerEasingFunction(float time);

	public Transform Target { get; protected set; }
	public TweenerEasingFunction EasingFunction { get; set; }
	protected float duration;
	protected bool started = false;
	protected bool stopped = false;
	protected float time;

	// Position
	protected Vector3 fromPosition;
	protected Vector3? toPositionOffset;
	protected bool useLocalPosition;

	// Euler
	protected Vector3 fromEuler;
	protected Vector3? toEulerOffset;
	protected bool useLocalEuler;

	protected Action completionHandler;


	//------------------------------------------------------------------------------------------------------
	//
	//  Constructor
	//
	//------------------------------------------------------------------------------------------------------

	public Tweener(Transform target, float duration) {
		this.Target = target;
		this.duration = duration;
		this.EasingFunction = TweenerEasingFunctions.Linear;
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public static void StopAllTweeners(Transform target) {
		TweenerHost.Current.StopAllTweeners(target);
	}

	public void SetPosition(Vector3? fromPosition, Vector3 toPosition, bool useLocalPosition) {
		this.useLocalPosition = useLocalPosition;
		if (fromPosition != null) {
			this.fromPosition = fromPosition.Value;
		} else {
			this.fromPosition = useLocalPosition ? this.Target.localPosition : this.Target.position;
		}
		this.toPositionOffset = toPosition - this.fromPosition;
	}

	public void SetEuler(Vector3? fromEuler, Vector3 toEuler, bool useLocalEuler, bool shortestWay) {
		this.useLocalEuler = useLocalEuler;
		if (fromEuler != null) {
			this.fromEuler = fromEuler.Value;
		} else {
			this.fromEuler = useLocalEuler ? this.Target.localEulerAngles : this.Target.eulerAngles;
		}
		if (shortestWay) {
			this.fromEuler = this.cleanUpAngles(this.fromEuler);
			this.toEulerOffset = this.shortestOffset(this.fromEuler, this.cleanUpAngles(toEuler));
		} else {
			this.toEulerOffset = toEuler - this.fromEuler;
		}
	}

	public void SetCompletionHandler(Action completionHandler) {
		this.completionHandler = completionHandler;
	}

	public void Start() {
		TweenerHost.Current.AddTweener(this);
		this.started = true;
	}

	public void Stop() {
		TweenerHost.Current.RemoveTweener(this);
		this.stopped = true;
	}

	public void Update() {
		this.time = Mathf.Min(this.time + Time.deltaTime, this.duration);
		if (this.started && !this.stopped) {
			float phase = this.EasingFunction(this.time / this.duration);

			// Position
			if (this.toPositionOffset != null) {
				if (this.useLocalPosition) {
					this.Target.localPosition = this.fromPosition + phase * this.toPositionOffset.Value;
				} else {
					this.Target.position = this.fromPosition + phase * this.toPositionOffset.Value;
				}
			}

			// Euler
			if (this.toEulerOffset != null) {
				if (this.useLocalEuler) {
					this.Target.localEulerAngles = this.fromEuler + phase * this.toEulerOffset.Value;
				} else {
					this.Target.eulerAngles = this.fromEuler + phase * this.toEulerOffset.Value;
				}
			}

			// Completion
			if (this.time >= this.duration) {
				this.Stop();
				this.completionHandler?.Invoke();
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected Vector3 cleanUpAngles(Vector3 angles) {
		angles.x = this.cleanUpAngle(angles.x);
		angles.y = this.cleanUpAngle(angles.y);
		angles.z = this.cleanUpAngle(angles.z);
		return angles;
	}

	protected float cleanUpAngle(float angle) {
		float cleanAngle = angle % 360.0f;
		return (cleanAngle < 0.0f) ? (cleanAngle % 360.0f) : cleanAngle;
	}

	protected Vector3 shortestOffset(Vector3 fromAngles, Vector3 toAngles) {
		Vector3 offset;
		offset.x = this.shortestOffset(fromAngles.x, toAngles.x);
		offset.y = this.shortestOffset(fromAngles.y, toAngles.y);
		offset.z = this.shortestOffset(fromAngles.z, toAngles.z);
		return offset;
	}

	protected float shortestOffset(float fromAngle, float toAngle) {
		float offset = toAngle - fromAngle;
		if (offset > 180.0f) {
			offset -= 360.0f;
		} else if (offset < -180.0f) {
			offset += 360.0f;
		}
		return offset;
	}
}
