﻿using UnityEngine;

public class TweenerEasingFunctions {


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public static float Linear(float time) {
		return time;
	}

	public static float QuadraticEaseIn(float time) {
		return time * time;
	}

	public static float QuadraticEaseOut(float time) {
		return 1f - Mathf.Pow(1f - time, 2f);
	}

	public static float QuadraticEaseInOut(float time) {
		float _time = time * 2f;
		return (_time < 1f ? QuadraticEaseIn(_time) : (QuadraticEaseOut(_time - 1f) + 1f)) * 0.5f;
	}
}
