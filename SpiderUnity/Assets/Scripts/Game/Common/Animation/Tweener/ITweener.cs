﻿using UnityEngine;

public interface ITweener {

	Transform Target { get; }
	void Stop();
	void Update();
}
