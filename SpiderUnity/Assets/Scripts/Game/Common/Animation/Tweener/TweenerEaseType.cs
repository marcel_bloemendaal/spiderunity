﻿public enum TweenerEaseType {
	None,
	In,
	Out,
	InOut
}
