﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteSwapLoop : MonoBehaviour {
	public Sprite[] Sprites;
	public float SwapInterval;

	protected SpriteRenderer spriteRenderer;
	protected int spriteIndex;
	protected float currentTime;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.spriteRenderer = this.GetComponent<SpriteRenderer>();
	}

	protected void Update() {
		this.currentTime += Time.deltaTime;

		// We do not use Floor as casting to int already has that effect (it ignores everything after the decimal point)
		int delta = (int)(this.currentTime / this.SwapInterval);
		this.currentTime = this.currentTime % this.SwapInterval;
		int desiredSpriteIndex = (this.spriteIndex + delta) % this.Sprites.Length;
		if (this.spriteIndex != desiredSpriteIndex) {
			this.spriteIndex = desiredSpriteIndex;
			this.spriteRenderer.sprite = this.Sprites[this.spriteIndex];
		}
	}
}
