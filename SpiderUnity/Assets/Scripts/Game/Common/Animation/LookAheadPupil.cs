﻿using UnityEngine;

public class LookAheadPupil : MonoBehaviour{
	public float MinXOffset;
	public float MaxXOffset;

	protected Vector3 basePosition;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	// A factor ranging from -1.0 to 1.0
	public void SetOffsetFactor(float factor) {
		Vector3 localPosition = this.transform.localPosition;
		localPosition.x = this.basePosition.x + ((factor > 0.0f) ? (factor * this.MaxXOffset) : (-factor * this.MinXOffset));
		this.transform.localPosition = localPosition;
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.basePosition = this.transform.localPosition;
	}
}
