﻿using UnityEngine;
using System.Collections;

public class RotateToVelocity : MonoBehaviour {

	public enum VelocityAxis {
		Horizontal,
		Vertical
	}

	[SerializeField] protected VelocityAxis velocityAxis = VelocityAxis.Horizontal;
	[SerializeField] protected float velocityRange = 10.0f; // In units (meters) per second
	[SerializeField] protected float maxZRotationOffset = 10.0f; // Degrees

	protected Vector3 previousPosition;


	//------------------------------------------------------------------------------------------------------
	//
	//  MonoBehaviour lif cycle
	//
	//------------------------------------------------------------------------------------------------------

	protected void Awake() {
		this.previousPosition = this.transform.position;
	}

	protected void Update() {
		if (Time.deltaTime > 0.0f) {
			Vector3 velocity = (this.transform.position - this.previousPosition) / Time.deltaTime;
			float targetVelocity = Mathf.Clamp(this.velocityAxis == VelocityAxis.Horizontal ? velocity.x : velocity.y, -this.velocityRange, this.velocityRange);
			Vector3 euler = this.transform.localEulerAngles;
			euler.z = (targetVelocity / this.velocityRange) * maxZRotationOffset;
			this.transform.localEulerAngles = euler;
			this.previousPosition = this.transform.position;
		}
	}
}
