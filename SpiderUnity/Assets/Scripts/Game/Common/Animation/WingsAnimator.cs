﻿using UnityEngine;

public class WingsAnimator : MonoBehaviour {

	[SerializeField] protected float cycleDuration = 1.0f;
	[SerializeField] protected bool randomizeStartPhase = false;
	[SerializeField] protected AnimatedWingGroup[] wingGroups;

	protected float angle;
	protected float angleSpeed;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void Awake() {
		this.angle = this.randomizeStartPhase ? Random.Range(0.0f, Mathf.PI * 2.0f) : 0.0f;
		this.angleSpeed = Mathf.PI * 2.0f / this.cycleDuration;

	}

	protected void Update() {

		this.angle = (this.angle + Time.deltaTime * this.angleSpeed) % (Mathf.PI * 2.0f);

		// Set angleOffsets of wing groups
		foreach (AnimatedWingGroup wingGroup in this.wingGroups) {
			float groupAngle = this.angle + wingGroup.PhaseOffset * Mathf.PI * 2.0f;
			wingGroup.ZRotationOffset = Mathf.Sin(groupAngle) * wingGroup.MaxZRotationOffset;
		}
	}
}

[System.Serializable]
public class AnimatedWing {
	public Transform Transform;

	protected float baseZRotation;


	public float ZRotationOffset {
		get {
			return this.Transform.localEulerAngles.z - this.baseZRotation;
		}
		set {
			Vector3 euler = this.Transform.localEulerAngles;
			euler.z = this.baseZRotation + value;
			this.Transform.localEulerAngles = euler;
		}
	}
}

[System.Serializable]
public class AnimatedWingGroup {
	[SerializeField] protected float maxZRotationOffset;
	[SerializeField] protected float phaseOffset;
	[SerializeField] protected AnimatedWing leftWing;
	[SerializeField] protected AnimatedWing rightWing;


	public float MaxZRotationOffset => this.maxZRotationOffset;
	public float PhaseOffset => this.phaseOffset;

	protected float zRotationOffset;

	public float ZRotationOffset {
		get {
			return this.zRotationOffset;
		}
		set {
			this.zRotationOffset = value;
			this.leftWing.ZRotationOffset = this.zRotationOffset;
			this.rightWing.ZRotationOffset = -this.zRotationOffset;
		}
	}
}
