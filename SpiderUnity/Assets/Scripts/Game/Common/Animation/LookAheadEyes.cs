﻿using UnityEngine;

public class LookAheadEyes : MonoBehaviour {

	protected const float MaxOffsetSpeed = 3.0f;
	protected const float MaxOffsetFactorChangePerSec = 0.05f;

	[SerializeField] protected LookAheadPupil[] pupils;

	protected float previousX;
	protected float offsetFactor;



	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Start() {
		this.previousX = this.transform.position.x;
	}

	protected void Update() {
		if (Time.deltaTime > 0.0f) {
			float speed = (this.transform.position.x - this.previousX) / Time.deltaTime;
			float desiredOffsetFactor = Mathf.Clamp(speed / MaxOffsetSpeed, -1.0f, 1.0f);
			this.offsetFactor = Mathf.Clamp(desiredOffsetFactor, this.offsetFactor - MaxOffsetFactorChangePerSec, this.offsetFactor + MaxOffsetFactorChangePerSec);
			foreach(LookAheadPupil pupil in this.pupils) {
				pupil.SetOffsetFactor(this.offsetFactor);
			}
			this.previousX = this.transform.position.x;
		}
	}
}
