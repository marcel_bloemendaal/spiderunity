﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour, ICheckpointEntity {
	
	public float TopMarginFactor = 0.25f;
	public float BottomMarginFactor = 0.25f;
	public float AwaySideMarginFactor = 0.25f;
	public float TowardSideMarginFactor = 0.4f;

	public float MaxSpeed = 15.0f;
	public float MaxSpeedChange = 15.0f;

	public float MinViewableX = 0.0f;
	public float MaxViewableX = 10.0f;
	public float MinViewableY = 0.0f;
	public float MaxViewableY = 10.0f;

	public Transform Target;

	protected new Camera camera;
	protected float previousTargetX;
	protected Vector3 previousSpeed;
	protected Vector3 originalPosition;
	protected Vector3? checkpointPosition;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  ICheckpointEntity
	//--------------------------------------------------

	public void ResetToOriginalState() {
		this.resetToPosition(this.originalPosition);
		this.checkpointPosition = null;
	}

	public void SaveCheckpoint(Firefly firefly) {
		Vector3 position = firefly.transform.position;
		this.checkpointPosition = new Vector3(position.x, position.y, this.transform.position.z);
	}

	public void ResetToCheckpoint() {
		this.resetToPosition(this.checkpointPosition == null ? this.originalPosition : this.checkpointPosition.Value);
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.camera = this.GetComponent<Camera>();
		this.originalPosition = this.transform.position;
	}

	protected void Start() {
		Environment.Current.RegisterCheckpointEntity(this);
		this.previousTargetX = this.Target.position.x;
		this.previousSpeed = Vector3.zero;
	}

	protected void OnDestroy() {
		Environment.Current?.UnregisterCheckpointEntity(this);
	}

	protected void Update() {
		if (Time.deltaTime > 0.0f) {
			Vector3 targetPosition = this.Target.position;
			float targetXSpeed = targetPosition.x - this.previousTargetX;
			this.previousTargetX = targetPosition.x;

			// Find world margins
			Vector3 min = this.camera.ScreenToWorldPoint(Vector3.zero);
			Vector3 max = this.camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
			float viewedWidth = max.x - min.x;
			float viewedHeight = max.y - min.y;
			float minTargetX;
			float maxTargetX;
			if (targetXSpeed > 0.0f) {
				minTargetX = min.x + AwaySideMarginFactor * viewedWidth;
				maxTargetX = max.x - TowardSideMarginFactor * viewedWidth;
			} else if (targetXSpeed < 0.0f) {
				minTargetX = min.x + TowardSideMarginFactor * viewedWidth;
				maxTargetX = max.x - AwaySideMarginFactor * viewedWidth;
			} else {
				minTargetX = min.x + AwaySideMarginFactor * viewedWidth;
				maxTargetX = max.x - AwaySideMarginFactor * viewedWidth;
			}
			float minTargetY = min.y + BottomMarginFactor * viewedHeight;
			float maxTargetY = max.y - TopMarginFactor * viewedHeight;

			// Find min / max positions
			float halfWidth = viewedWidth * 0.5f;
			float halfHeiht = viewedHeight * 0.5f;
			float minX = this.MinViewableX + halfWidth;
			float maxX = this.MaxViewableX - halfWidth;
			float minY = this.MinViewableY + halfHeiht;
			float maxY = this.MaxViewableY - halfHeiht;

			Vector3 desiredPosition = this.camera.transform.position;
			if (targetPosition.x < minTargetX) {
				desiredPosition.x = this.camera.transform.position.x + targetPosition.x - minTargetX;
			} else if (targetPosition.x > maxTargetX) {
				desiredPosition.x = this.camera.transform.position.x + targetPosition.x - maxTargetX;
			}
			if (targetPosition.y < minTargetY) {
				desiredPosition.y = this.camera.transform.position.y + targetPosition.y - minTargetY;
			} else if (targetPosition.y > maxTargetY) {
				desiredPosition.y = this.camera.transform.position.y + targetPosition.y - maxTargetY;
			}
			Vector3 desiredPositionChange = desiredPosition - this.camera.transform.position;

			Vector3 speedChange = desiredPositionChange / Time.deltaTime - this.previousSpeed;
			float mag = speedChange.magnitude;
			if (mag > this.MaxSpeedChange) {
				speedChange *= (this.MaxSpeedChange / mag);
			}
			Vector3 position = this.camera.transform.position;
			Vector3 newPosition = position + (this.previousSpeed + speedChange) * Time.deltaTime;
			newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
			newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);
			this.camera.transform.position = newPosition;
			this.previousSpeed = (this.camera.transform.position - position) / Time.deltaTime;
		}
	}

	protected void resetToPosition(Vector3 position) {
		this.transform.position = position;
		this.previousSpeed = Vector3.zero;
	}
}
