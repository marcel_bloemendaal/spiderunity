﻿using UnityEngine;
public class SpiderCheckpointState {

	public Vector3 Position { get; protected set; }
	public int Health { get; protected set; }
	public int NumberOfFliesCollected { get; protected set; }


	//------------------------------------------------------------------------------------------------------
	//
	//  Constructor
	//
	//------------------------------------------------------------------------------------------------------

	public SpiderCheckpointState(Vector3 position, int health, int numberOfFliesCollected) {
		this.Position = position;
		this.Health = health;
		this.NumberOfFliesCollected = numberOfFliesCollected;
	}
}
