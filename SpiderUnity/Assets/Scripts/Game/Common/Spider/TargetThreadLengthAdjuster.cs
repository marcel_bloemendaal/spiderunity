﻿using UnityEngine;


public class TargetThreadLengthAdjuster : MonoBehaviour, ITouchResponder {

	protected const float TouchRadiusInches = 0.4f;

	protected SpiderThreadShooter spiderThreadShooter;
	protected Vector3 touchStartPosition;
	protected float startTargetThreadLength;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  touches
	//---------------------------------------------------

	public TouchDownResponse OnTouchDown(Vector2 position) {
		TouchDownResponse response = TouchDownResponse.Ignore;
		if (this.spiderThreadShooter.CurrentThread != null) {
			Vector2 screenPosition = Camera.main.WorldToScreenPoint(this.transform.TransformPoint(Spider.centerOffset));
			if (((screenPosition - position).magnitude / Screen.dpi) <= TouchRadiusInches) {
				this.touchStartPosition = Camera.main.ScreenToWorldPoint(position);
				this.touchStartPosition.z = 0.0f;
				this.startTargetThreadLength = this.spiderThreadShooter.CurrentThread.Length;
				response = TouchDownResponse.ClaimTouch;
			}
		}
		return response;
	}

	public void OnTouchMove(Vector2 position) {
		if (this.spiderThreadShooter.CurrentThread != null) {
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(position);
			worldPosition.z = 0.0f;
			this.spiderThreadShooter.SetTargetThreadLength(this.startTargetThreadLength - (worldPosition.y - this.touchStartPosition.y), true);
		}
	}

	public void OnTouchUp(Vector2 position) {
		// Nothing, but required by ITouchResponder
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.spiderThreadShooter = this.gameObject.GetComponent<SpiderThreadShooter>();
	}

	protected void Start() {
		SpiderTouchControlSurface.Current.AddTouchResponder(this);
	}

	protected void OnDestroy() {
		SpiderTouchControlSurface controlSurface = SpiderTouchControlSurface.Current;
		if (controlSurface != null) {
			controlSurface.RemoveTouchResponder(this);
		}
	}
}
