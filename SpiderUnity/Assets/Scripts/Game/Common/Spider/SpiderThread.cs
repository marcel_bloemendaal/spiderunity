﻿using UnityEngine;
using UnityEngine.Assertions;

public class SpiderThread : MonoBehaviour, ICheckpointEntity {
	public delegate void AttachedHandler(SpiderThread thread);
	public delegate void BrokenHandler(SpiderThread thread);

	public event AttachedHandler Attached;
	public event BrokenHandler Broken;

	public const float MinLength = 0.5f;
	public const float MaxLength = 20.0f;
	public const float TemporaryThreadLifeTime = 10.0f;
	public const float MinNewThreadDistance = 0.001f;

	protected const float ShootingSpeed = 30.0f;

	public ThreadState State { get; protected set; }

	public Transform Appearance;

	[SerializeField]
	protected SpriteRenderer spriteRenderer;

	protected Transform origin;
	protected float shootingAngle;
	protected SpiderThreadFactory spiderThreadFactory;
	protected ICollector collector;
	protected bool isOffSpiderThread;
	protected float spriteLength;
	protected DistanceJoint2D distanceJoint;
	protected Vector3 endPoint;
	protected LayerMask layerMask;
	protected float lifeTime = TemporaryThreadLifeTime;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void Initialize(Transform origin, Vector3 localAnchor, float shootingAngle, SpiderThreadFactory spiderThreadFactory, ICollector collector, bool isOffSpiderThread) {
		Assert.IsNull(this.origin, "SpiderThread may only be initialized once!");
		this.origin = origin;
		this.shootingAngle = shootingAngle;
		this.spiderThreadFactory = spiderThreadFactory;
		this.collector = collector;
		this.isOffSpiderThread = isOffSpiderThread;

		this.transform.SetParent(this.origin, false);
		this.transform.localPosition = localAnchor;
		this.endPoint = this.origin.transform.position;
		this.State = ThreadState.Shooting;
	}

	public void Initialize(Transform origin, Vector3 localAnchor, Rigidbody2D connectedBody, Vector3 connectedLocalAnchor, SpiderThreadFactory spiderThreadFactory, ICollector collector, bool isTemporaryThread) {
		Assert.IsNull(this.origin, "SpiderThread may only be initialized once!");
		this.origin = origin;
		this.spiderThreadFactory = spiderThreadFactory;
		this.collector = collector;
		this.isOffSpiderThread = isTemporaryThread;

		this.transform.SetParent(this.origin, false);
		this.transform.localPosition = localAnchor;
		this.endPoint = this.origin.transform.position;
		this.distanceJoint = this.origin.gameObject.AddComponent<DistanceJoint2D>();
		this.distanceJoint.autoConfigureDistance = true;
		this.distanceJoint.autoConfigureConnectedAnchor = false;
		this.distanceJoint.enableCollision = true;
		this.distanceJoint.maxDistanceOnly = true;

		if (connectedBody.transform != origin) {
			this.distanceJoint.connectedBody = connectedBody;
			this.distanceJoint.connectedAnchor = connectedLocalAnchor;
		}
		this.endPoint = connectedBody.transform.TransformPoint(connectedLocalAnchor);
		this.State = ThreadState.Attached;
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public float Length {
		get {
			return this.State == ThreadState.Attached && this.distanceJoint.connectedBody != null ? this.distanceJoint.distance : (this.endPoint - this.transform.position).magnitude;
		}
		set {
			if (this.State == ThreadState.Attached) {
				this.distanceJoint.distance = value;
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  IStateEntity
	//--------------------------------------------------

	public void ResetToOriginalState() {
		Destroy(this.gameObject);
	}

	public void SaveCheckpoint(Firefly firefly) {
		// Do nothing, method just required by interface
	}

	public void ResetToCheckpoint() {
		Destroy(this.gameObject);
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.spriteLength = this.spriteRenderer.sprite.bounds.size.x;
		this.layerMask = ~LayerMask.GetMask(Layers.Spider);
		Vector3 localScale = this.spriteRenderer.transform.localScale;
		localScale.x = 0.0f;
		this.spriteRenderer.transform.localScale = localScale;
	}

	protected void Start() {
		Environment.Current.RegisterCheckpointEntity(this);
	}

	protected void FixedUpdate() {

		if (this.isOffSpiderThread) {
			this.lifeTime -= Time.fixedDeltaTime;
		}

		if (this.lifeTime <= 0.0f) {
			Destroy(this.gameObject);
		} else {
			if (this.State == ThreadState.Shooting) {

				// Move endpoint
				float deltaLength = Time.fixedDeltaTime * ShootingSpeed;
				this.endPoint.Set(this.endPoint.x + Mathf.Cos(this.shootingAngle) * deltaLength, this.endPoint.y + Mathf.Sin(this.shootingAngle) * deltaLength, 0.0f);
			} else {
				this.endPoint = this.distanceJoint.connectedBody.transform.TransformPoint(this.distanceJoint.connectedAnchor);
			}

			// Raycast
			Vector3 threadVector = this.endPoint - this.transform.position;
			float length = threadVector.magnitude;
			float rayCastDistance = this.State == ThreadState.Attached ? length - MinNewThreadDistance : Mathf.Min(length, MaxLength);
			RaycastHit2D[] hits;
			if (this.isOffSpiderThread) {
				hits = Physics2D.RaycastAll(this.transform.position, threadVector.normalized, rayCastDistance);
			} else {
				hits = Physics2D.RaycastAll(this.transform.position, threadVector.normalized, rayCastDistance, this.layerMask);
			}
			foreach (RaycastHit2D hit in hits) {
				if (hit.collider != null) {
					bool stick = true;
					bool hitCheckDone = true;
					IThreadResponder threadResponder = hit.collider.GetComponent<IThreadResponder>();
					if (threadResponder != null) {
						switch (threadResponder.RespondToThread()) {
							case ThreadResponse.Ignore:
								stick = false;
								hitCheckDone = false;
								break;

							case ThreadResponse.Stick:
								if (this.State == ThreadState.Shooting || (hit.distance > MinNewThreadDistance && (length - hit.distance) > MinNewThreadDistance)) {
									stick = true;
									hitCheckDone = true;
								} else {
									stick = false;
									hitCheckDone = false;
								}
								break;

							case ThreadResponse.Break:
								stick = false;
								hitCheckDone = true;
								Destroy(this.gameObject);
								break;

							case ThreadResponse.GetCollected:
								stick = false;
								hitCheckDone = false;
								if (this.collector != null) {
									Collectable collectable = hit.collider.GetComponent<Collectable>();
									if (collectable != null) {
										collectable.GetCollected(collector);
									}
								}
								break;
						}
					} else {
						if (this.State == ThreadState.Shooting || (hit.distance > MinNewThreadDistance && (length - hit.distance) > MinNewThreadDistance)) {
							stick = true;
							hitCheckDone = true;
						} else {
							stick = false;
							hitCheckDone = false;
						}
					}
					if (stick) {
						switch (this.State) {
							case ThreadState.Shooting:
								this.distanceJoint = this.origin.gameObject.AddComponent<DistanceJoint2D>();
								this.distanceJoint.autoConfigureDistance = false;
								this.distanceJoint.autoConfigureConnectedAnchor = false;
								this.distanceJoint.connectedBody = hit.rigidbody;
								this.distanceJoint.connectedAnchor = hit.transform.InverseTransformPoint(hit.point);
								this.distanceJoint.distance = hit.distance;
								this.distanceJoint.enableCollision = true;
								this.distanceJoint.maxDistanceOnly = true;
								this.endPoint = hit.point;
								length = hit.distance;
								this.State = ThreadState.Attached;
								this.Attached?.Invoke(this);
								break;

							case ThreadState.Attached:

								/* Split this thread at the collision point. This thread stay attached to its
								 * origin with the collision point as its endPoint, a new thread will spawn with
								 * the collision point as its origin and the current endPoint as its end point
								 */

								// 1. If the collision point is not on the same body as the endpoint, create a new
								// thread from the collision point to the previousEndPoint
								if (hit.rigidbody != this.distanceJoint.connectedBody) {
									SpiderThread newThread = this.spiderThreadFactory.CreateSpiderThread();
									newThread.Initialize(hit.transform, hit.transform.InverseTransformPoint(hit.point), this.distanceJoint.connectedBody, this.distanceJoint.connectedAnchor, this.spiderThreadFactory, this.collector, true);
								}

								// 2. If the collision point is not on the same body as the origin, set the
								// collision point as this thread's new endPoint. Otherwise destroy this thread.
								if (hit.rigidbody.gameObject != this.origin.gameObject) {
									this.distanceJoint.connectedBody = hit.rigidbody;
									this.distanceJoint.connectedAnchor = hit.transform.InverseTransformPoint(hit.point);
									this.distanceJoint.distance = hit.distance;
									this.endPoint = hit.point;
									length = hit.distance;
								} else {
									Destroy(this.gameObject);
								}
								break;
						}
					}
					if (hitCheckDone) {
						break;
					}
				}
			}
			if (length > MaxLength) {
				Destroy(this.gameObject);
			}
		}
	}

	protected void Update() {
		this.Appearance.position = this.endPoint;

		Vector3 localScale = this.spriteRenderer.transform.localScale;
		localScale.x = (this.endPoint - this.transform.position).magnitude / this.spriteLength;
		Vector3 euler = this.spriteRenderer.transform.eulerAngles;
		euler.z = Mathf.Atan2(this.transform.position.y - this.endPoint.y, this.transform.position.x - this.endPoint.x) * Mathf.Rad2Deg;
		this.Appearance.transform.eulerAngles = euler;
		this.spriteRenderer.transform.localScale = localScale;
	}

	protected void OnDestroy() {
		Environment.Current.UnregisterCheckpointEntity(this);
		if (this.distanceJoint != null) {
			Destroy(this.distanceJoint);
		}
		this.Broken?.Invoke(this);
	}
}
