﻿using UnityEngine;

[RequireComponent(typeof(SpiderThreadShooter))]
public class SpiderLegs : MonoBehaviour {

	public const float CurlAnimationDuration = 0.1f;

	protected const float MaxSwingRotationSpeed = 3.0f;
	protected const float MaxSwingFactorChangePerSec = 0.05f;

	[SerializeField]
	protected SpiderSwingingLeg[] swingingLegs;
	[SerializeField]
	protected SpiderClimbingLeg leftClimbingLeg;
	[SerializeField]
	protected SpiderClimbingLeg rightClimbingLeg;

	protected float previousX;
	protected float swingFactor;
	protected float climbAngle;
	protected float climbAngleSpeed;
	protected SpiderThreadShooter spiderThreadShooter;

	protected bool spiderAttached = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.spiderThreadShooter = this.gameObject.GetComponent<SpiderThreadShooter>();
		this.spiderAttached = this.spiderThreadShooter.CurrentThread != null && this.spiderThreadShooter.CurrentThread.State == ThreadState.Attached;
		this.spiderThreadShooter.CurrentThreadAttached += this.onCurrentThreadAttached;
		this.spiderThreadShooter.CurrentThreadBroken += this.onCurrentThreadBroken;
	}

	protected void Start() {
		this.previousX = this.transform.position.x;
		this.climbAngleSpeed = Mathf.PI / ((2.0f * SpiderClimbingLeg.ClimbRadius) / SpiderThreadShooter.LengthChangeSpeed);

		foreach (SpiderSwingingLeg spiderLeg in this.swingingLegs) {
			spiderLeg.Initialize();
		}

		this.leftClimbingLeg.Initialize(true);
		this.rightClimbingLeg.Initialize(false);
	}

	protected void Update() {
		if (Time.deltaTime > 0.0f) {
			if (this.spiderAttached) {

				// Leg swinging
				float speed = (this.transform.position.x - this.previousX) / Time.deltaTime;
				float desiredFactor = Mathf.Clamp(speed / MaxSwingRotationSpeed, -1.0f, 1.0f);
				this.swingFactor = Mathf.Clamp(desiredFactor, this.swingFactor - MaxSwingFactorChangePerSec, this.swingFactor + MaxSwingFactorChangePerSec);
				foreach (SpiderSwingingLeg spiderLeg in this.swingingLegs) {
					spiderLeg.SetSwingFactor(this.swingFactor);
				}

				// Leg climbing
				if (this.spiderThreadShooter.ClimbingType != SpiderClimbingType.None) { 
					float factor = this.spiderThreadShooter.ClimbingType == SpiderClimbingType.ClimbingUp ? 1.0f : -1.0f;
					float twoPI = Mathf.PI * 2.0f;
					this.climbAngle += (factor * climbAngleSpeed * Time.deltaTime);
					this.climbAngle = this.climbAngle % twoPI;
					if (this.climbAngle < 0.0f) {
						this.climbAngle += twoPI;
					}

					this.leftClimbingLeg.SetClimbAngle(this.climbAngle);
					this.rightClimbingLeg.SetClimbAngle((this.climbAngle + Mathf.PI) % twoPI);
				}
			}

			this.previousX = this.transform.position.x;
		}
	}

	protected void OnDestroy() {
		this.spiderThreadShooter.CurrentThreadAttached -= this.onCurrentThreadAttached;
		this.spiderThreadShooter.CurrentThreadBroken -= this.onCurrentThreadBroken;
	}

	//--------------------------------------------------
	//  Events
	//--------------------------------------------------

	protected void onCurrentThreadAttached(SpiderThread thread) {
		foreach (SpiderSwingingLeg swingingLeg in this.swingingLegs) {
			swingingLeg.State = SpiderSwingingLeg.LegState.Swinging;
		}
		this.leftClimbingLeg.State = SpiderClimbingLeg.LegState.Climbing;
		this.rightClimbingLeg.State = SpiderClimbingLeg.LegState.Climbing;
		this.spiderAttached = true;
	}

	protected void onCurrentThreadBroken(SpiderThread thread) {
		foreach (SpiderSwingingLeg swingingLeg in this.swingingLegs) {
			swingingLeg.State = SpiderSwingingLeg.LegState.CurledUp;
		}
		this.leftClimbingLeg.State = SpiderClimbingLeg.LegState.CurledUp;
		this.rightClimbingLeg.State = SpiderClimbingLeg.LegState.CurledUp;
		this.spiderAttached = false;
	}
}
