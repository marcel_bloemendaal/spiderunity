﻿using UnityEngine;
using System.Collections;

public class SpiderThreadFactory : MonoBehaviour {
	[SerializeField]
	protected GameObject spiderThreadPrefab;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public SpiderThread CreateSpiderThread() {
		return Instantiate(this.spiderThreadPrefab).GetComponent<SpiderThread>();
	}
}
