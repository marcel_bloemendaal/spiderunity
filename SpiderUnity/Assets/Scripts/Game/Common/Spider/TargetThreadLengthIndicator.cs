﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class TargetThreadLengthIndicator : MonoBehaviour {

	protected const float TotalAngle = Mathf.PI * 0.25f;
	protected const float MaxSegmentLength = 0.05f;
	protected const float LineWidth = 0.02f;
	protected const float StartAngle = -TotalAngle * 0.5f;
	protected const float DefaultAlpha = 0.5f;
	protected const float RadiusOffset = 0.25f;


	protected float radius;
	protected LineRenderer lineRenderer;
	protected Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public float TargetLength {
		get {
			return this.radius;
		}
		set {
			this.radius = value + RadiusOffset;
			this.updateLine();
		}
	}

	public float Alpha {
		get {
			return this.lineRenderer.startColor.a;
		}
		set {
			this.color.a = value * DefaultAlpha;
			Material material = this.lineRenderer.material;
			material.color = this.color;
			this.lineRenderer.material = material;
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.lineRenderer = this.gameObject.GetComponent<LineRenderer>();
		this.lineRenderer.startWidth = LineWidth;
		this.lineRenderer.endWidth = LineWidth;
	}

	//---------------------------------------------------
	//  Other
	//---------------------------------------------------

	protected void updateLine() {
		Vector3 center = this.transform.localPosition;
		float arcLength = this.radius * TotalAngle;
		int numSegments = (int)Mathf.Ceil(arcLength / MaxSegmentLength);
		float segmentAngle = TotalAngle / numSegments;
		Vector3[] positions = new Vector3[numSegments + 1];
		for (int index = 0; index <= numSegments; ++index) {
			float angle = StartAngle + index * segmentAngle;
			positions[index] = new Vector3(center.x + Mathf.Cos(angle) * this.radius, center.y + Mathf.Sin(angle) * this.radius, 0.0f);
		}
		this.lineRenderer.positionCount = positions.Length;
		this.lineRenderer.SetPositions(positions);
	}
}
