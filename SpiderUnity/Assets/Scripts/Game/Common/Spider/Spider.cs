﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpiderThreadShooter))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Spider : MonoBehaviour, IVulnerable, ICollector, ICheckpointEntity {

	public delegate void NumberOfFliesCollectedChangedHandler(int numberOfFliesCollected);

	public event NumberOfFliesCollectedChangedHandler NumberOfFliesCollectedChanged;

	public static Vector3 centerOffset = new Vector3(0.0f, -0.2f, 0.0f);

	protected const int FullHealth = 3;
	protected const int SecondsOfInvincibilityAfterDamage = 2;

	public SpiderThreadShooter ThreadShooter { get; protected set; }

	[SerializeField] protected GameObject bandAid;
	[SerializeField] protected GameObject blackEye;
	[SerializeField] protected GameObject graphicsContainer;
	[SerializeField] protected Transform bonesContainer;

	protected Animator animator;
	new protected Rigidbody2D rigidbody;
	protected CircleCollider2D circleCollider;

	public int Health { get; protected set; }

	protected bool canReceiveDamage = true;
	protected List<SpiderBone> bones = new List<SpiderBone>();
	protected int numberOfFliesCollected;

	protected SpiderCheckpointState originalState;
	protected SpiderCheckpointState checkpointState;


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public int NumberOfFliesCollected {
		get {
			return this.numberOfFliesCollected;
		}
		set {
			this.numberOfFliesCollected = value;
			this.NumberOfFliesCollectedChanged?.Invoke(this.numberOfFliesCollected);
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  IVulnerable
	//--------------------------------------------------

	public void ReceiveDamage(int amount) {
		if (this.canReceiveDamage && this.Health > 0) {
			this.Health = Mathf.Max(this.Health - amount, 0);
			if (this.Health == 0) {
				this.die();
			} else {
				this.updateAppearance();
				this.StartCoroutine(this.processBeingDamaged());
			}
		}
	}

	public void ReceiveLethalDamage() {
		if (this.Health > 0) {
			this.Health = 0;
			this.die();
		}
	}

	//--------------------------------------------------
	//  ICollector
	//--------------------------------------------------

	public void CollectHealth(int amount) {
		this.Health = Mathf.Clamp(this.Health + amount, 0, FullHealth);
		this.updateAppearance();
	}

	public void CollectFullHealth() {
		this.Health = FullHealth;
		this.updateAppearance();
	}

	public void CollectFly() {
		++this.NumberOfFliesCollected;
	}

	//--------------------------------------------------
	//  ICheckpointEntity
	//--------------------------------------------------

	public void ResetToOriginalState() {
		this.resetToState(this.originalState);
		this.checkpointState = null;
	}

	public void SaveCheckpoint(Firefly firefly) {
		Vector3 position = firefly.transform.position;
		this.checkpointState = new SpiderCheckpointState(new Vector3(position.x, position.y - SpiderThreadShooter.DefaultTargetThreadLength, 0.0f), this.Health, this.numberOfFliesCollected);
	}

	public void ResetToCheckpoint() {
		this.resetToState(this.checkpointState == null ? this.originalState : this.checkpointState);
	}


	//--------------------------------------------------
	//  Finish level
	//--------------------------------------------------

	public void PlayVictoryAnimation(Vector3 centerPosition) {
		this.setPhysicsEnabled(false);
		this.transform.position = centerPosition - centerOffset;
		this.transform.rotation = Quaternion.identity;
		this.animator.enabled = true;
		this.animator.Play("Victory");
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.Health = FullHealth;
		this.bandAid.SetActive(false);
		this.blackEye.SetActive(false);
		this.rigidbody = this.GetComponent<Rigidbody2D>();
		this.circleCollider = this.GetComponent<CircleCollider2D>();
		this.animator = this.GetComponent<Animator>();
		this.ThreadShooter = this.GetComponent<SpiderThreadShooter>();
		this.animator.enabled = false;
	}

	protected void Start() {
		Environment.Current.RegisterCheckpointEntity(this);

		// Proces bones
		foreach (SpiderBone bone in this.bonesContainer.GetComponentsInChildren<SpiderBone>()) {
			bone.ReferenceBody = this.rigidbody;
			bone.Reset();
			this.bones.Add(bone);
		}
		this.bonesContainer.gameObject.SetActive(false);

		// Save original state
		this.originalState = new SpiderCheckpointState(this.transform.position, this.Health, this.numberOfFliesCollected);
	}

	protected void OnDestroy() {
		Environment.Current?.UnregisterCheckpointEntity(this);
	}

	//---------------------------------------------
	//  Other
	//-----------------------------------------------

	protected void die() {
		this.bonesContainer.gameObject.SetActive(true);
		foreach(SpiderBone bone in this.bones) {
			bone.Spawn();
		}
		this.setPhysicsEnabled(false);
		this.graphicsContainer.SetActive(false);
		Environment.Current.Fail();
	}

	protected IEnumerator processBeingDamaged() {
		this.canReceiveDamage = false;
		for (int secondsLeft = SecondsOfInvincibilityAfterDamage; secondsLeft > 0; --secondsLeft) {
			for (int index = 0; index < 4; ++index) {
				GameObjectUtils.SetColorRecursively(this.graphicsContainer, Color.red);
				yield return new WaitForSeconds(0.125f);
				GameObjectUtils.SetColorRecursively(this.graphicsContainer, Color.white);
				yield return new WaitForSeconds(0.125f);
			}
		}
		this.canReceiveDamage = true;
	}

	protected void updateAppearance() {
		this.bandAid.SetActive(this.Health < FullHealth);
		this.blackEye.SetActive(this.Health < FullHealth - 1);
	}

	protected void setPhysicsEnabled(bool enabled) {
		if (enabled) {
			this.rigidbody.gravityScale = 1.0f;
		} else {
			if (this.ThreadShooter.CurrentThread != null) {
				this.ThreadShooter.BreakCurrentThread();
			}
			this.rigidbody.gravityScale = 0.0f;
			this.rigidbody.velocity = Vector3.zero;
			this.rigidbody.angularVelocity = 0f;
		}
		this.circleCollider.enabled = enabled;
		this.ThreadShooter.CanShootThread = enabled;
	}

	protected void resetToState(SpiderCheckpointState state) {
		this.setPhysicsEnabled(true);
		this.transform.position = state.Position;
		this.transform.rotation = Quaternion.identity;
		this.rigidbody.velocity = Vector2.zero;
		this.rigidbody.angularVelocity = 0.0f;
		this.ThreadShooter.ResetTargetThreadLengthToDefault();
		this.ThreadShooter.ShootThread(this.transform.position + Vector3.up);
		this.bonesContainer.gameObject.SetActive(false);
		foreach (SpiderBone bone in this.bones) {
			bone.Reset();
		}
		this.graphicsContainer.SetActive(true);
		this.Health = state.Health;
		this.NumberOfFliesCollected = state.NumberOfFliesCollected;
		this.updateAppearance();
	}

	//--------------------------------------------------
	//  Events
	//--------------------------------------------------

}
