﻿using UnityEngine;

[RequireComponent(typeof(SpiderThreadFactory))]
public class SpiderThreadShooter : MonoBehaviour {

	public delegate void ShotThreadHandler(SpiderThread thread);
	
	public event ShotThreadHandler ShotThread;
	public event SpiderThread.AttachedHandler CurrentThreadAttached;
	public event SpiderThread.AttachedHandler CurrentThreadBroken;

	public const float LengthChangeSpeed = 1.25f;
	public const float DefaultTargetThreadLength = 1.3f;

	protected const float TargetThreadLengthIndicatorDuration = 2.0f;

	public SpiderClimbingType ClimbingType { get; protected set; }

	[SerializeField]
	protected TargetThreadLengthIndicator targetThreadLengthIndicator;

	public SpiderThread CurrentThread { get; protected set; }

	public bool CanShootThread = true;

	protected SpiderThreadFactory spiderThreadFactory;
	protected float targetThreadLength = DefaultTargetThreadLength;
	protected float targetThreadLengthIndicatorDuration;
	protected ICollector collector;


	//------------------------------------------------------------------------------------------------------
	//
	//  Computed properties
	//
	//------------------------------------------------------------------------------------------------------

	public float TargetThreadLength {
		get {
			return this.targetThreadLength;
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void ShootThread(Vector3 targetPoint) {
		if (this.CanShootThread) {
			Vector3 delta = targetPoint - this.transform.position;

			// We can only shoot threads up, not down
			if (delta.y > 0.0f) {
				if (this.CurrentThread != null) {
					this.CurrentThread.Broken -= this.onCurrentThreadBroken;
					Destroy(this.CurrentThread.gameObject);
					this.ClimbingType = SpiderClimbingType.None;
				}

				this.CurrentThread = this.spiderThreadFactory.CreateSpiderThread();
				this.CurrentThread.Initialize(this.transform, Vector3.zero, Mathf.Atan2(delta.y, delta.x), this.spiderThreadFactory, this.collector, false);
				this.CurrentThread.Attached += this.onCurrentThreadAttached;
				this.CurrentThread.Broken += this.onCurrentThreadBroken;
				this.targetThreadLengthIndicator.transform.SetParent(this.CurrentThread.Appearance, false);
				this.clearTargetThreadLengthIndication();
				this.ShotThread?.Invoke(this.CurrentThread);
			}
		}
	}

	public void BreakCurrentThread() {
		if (this.CurrentThread != null) {
			Destroy(this.CurrentThread.gameObject);
		}
	}

	public void SetTargetThreadLength(float value, bool showIndicator) {
		this.targetThreadLength = Mathf.Clamp(value, SpiderThread.MinLength, SpiderThread.MaxLength);
		if (this.CurrentThread != null) {
			this.targetThreadLengthIndicator.TargetLength = this.targetThreadLength;
			this.targetThreadLengthIndicator.Alpha = 1.0f;
			this.targetThreadLengthIndicator.gameObject.SetActive(true);
			this.targetThreadLengthIndicatorDuration = showIndicator ? TargetThreadLengthIndicatorDuration : 0.0f;
			this.updateClimbingType();
		}
	}

	public void ResetTargetThreadLengthToDefault() {
		this.SetTargetThreadLength(DefaultTargetThreadLength, false);
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void Awake() {
		this.spiderThreadFactory = this.gameObject.GetComponent<SpiderThreadFactory>();
		this.ClimbingType = SpiderClimbingType.None;
		this.collector = this.GetComponent<ICollector>();
	}

	protected void Start() {
		this.ShootThread(this.transform.position + Vector3.up);
		this.targetThreadLengthIndicator.gameObject.SetActive(false);
	}

	protected void FixedUpdate() {
		if (this.CurrentThread != null) {
		
			// Change thread length if required
			if (!Mathf.Approximately(this.CurrentThread.Length, this.targetThreadLength)) {
				float delta = this.targetThreadLength - this.CurrentThread.Length;
				delta = (delta > 0.0f) ? Mathf.Min(delta, Time.fixedDeltaTime * LengthChangeSpeed) : Mathf.Max(delta, Time.fixedDeltaTime * -LengthChangeSpeed);
				this.CurrentThread.Length += delta;
			}
			this.updateClimbingType();
		}
	}

	protected void Update() {
		if (this.CurrentThread != null && this.CurrentThread.State == ThreadState.Attached && this.targetThreadLengthIndicatorDuration > 0.0f) {
			this.targetThreadLengthIndicatorDuration -= Time.deltaTime;
			if (this.targetThreadLengthIndicatorDuration > 0.0f) {
				this.targetThreadLengthIndicator.Alpha = this.targetThreadLengthIndicatorDuration / TargetThreadLengthIndicatorDuration;
			} else {
				this.targetThreadLengthIndicator.gameObject.SetActive(false);
			}
		}
	}

	protected void OnDestroy() {
		if (this.CurrentThread != null) {
			this.CurrentThread.Attached -= this.onCurrentThreadAttached;
			this.CurrentThread.Broken -= this.onCurrentThreadBroken;
		}
	}

	//---------------------------------------------------
	//  TargetThreadLengthIndicator
	//---------------------------------------------------

	protected void clearTargetThreadLengthIndication() {
		this.targetThreadLengthIndicator.gameObject.SetActive(false);
		this.targetThreadLengthIndicatorDuration = 0.0f;
	}

	protected void updateClimbingType() {
		if (this.CurrentThread == null || this.CurrentThread.State != ThreadState.Attached || Mathf.Approximately(this.targetThreadLength, this.CurrentThread.Length)) {
			this.ClimbingType = SpiderClimbingType.None;
		} else if (this.targetThreadLength > this.CurrentThread.Length) {
			this.ClimbingType = SpiderClimbingType.ClimbingDown;
		} else if (this.targetThreadLength < this.CurrentThread.Length) {
			this.ClimbingType = SpiderClimbingType.ClimbingUp;
		}
	}

	//---------------------------------------------------
	//  Events
	//---------------------------------------------------

	protected void onCurrentThreadAttached(SpiderThread thread) {
		this.CurrentThreadAttached?.Invoke(thread);
	}

	protected void onCurrentThreadBroken(SpiderThread thread) {
		this.CurrentThread = null;
		this.targetThreadLengthIndicator.transform.SetParent(this.transform, false);
		this.targetThreadLengthIndicator.gameObject.SetActive(false);
		this.clearTargetThreadLengthIndication();
		this.ClimbingType = SpiderClimbingType.None;
		this.CurrentThreadBroken?.Invoke(thread);
	}
}
