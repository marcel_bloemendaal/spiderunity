﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpiderBone : MonoBehaviour, IThreadResponder {

	public Rigidbody2D ReferenceBody { get; set; }

	[SerializeField] protected Transform referenceTransform;
	protected Rigidbody2D rigidBody;
	protected Vector3 referenceLocalOffset;
	protected Vector3 referenceEulerOffset;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void Reset() {
		this.rigidBody.bodyType = RigidbodyType2D.Kinematic;
		this.rigidBody.velocity = Vector2.zero;
		this.rigidBody.angularVelocity = 0f;
	}

	public void Spawn() {
		this.transform.position = this.referenceTransform.transform.TransformPoint(this.referenceLocalOffset);
		this.transform.eulerAngles = this.referenceTransform.transform.eulerAngles + this.referenceEulerOffset;
		this.rigidBody.velocity = this.ReferenceBody.velocity;
		this.rigidBody.angularVelocity = this.ReferenceBody.angularVelocity;
		this.rigidBody.bodyType = RigidbodyType2D.Dynamic;
		this.rigidBody.WakeUp();
	}

	//--------------------------------------------------
	//  IThreadResponder
	//--------------------------------------------------

	public ThreadResponse RespondToThread() {
		return ThreadResponse.Ignore;
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.rigidBody = this.GetComponent<Rigidbody2D>();
		this.rigidBody.bodyType = RigidbodyType2D.Kinematic;
	}

	protected void Start() {
		this.referenceLocalOffset = this.referenceTransform.transform.InverseTransformPoint(this.transform.position);
		this.referenceEulerOffset = this.transform.eulerAngles - this.referenceTransform.transform.eulerAngles;
	}
}
