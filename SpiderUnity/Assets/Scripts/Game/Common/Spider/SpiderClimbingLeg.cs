﻿using System;
using UnityEngine;

[Serializable]
public class SpiderClimbingLeg {

	public enum LegState {
		Climbing,
		CurledUp
	}

	public const float ClimbRadius = 0.08f;

	public Transform Inner;
	public Transform Outer;

	protected const float ClimbCenterY = 0.28f;
	protected const float HalfClimbCircumference = ClimbRadius * Mathf.PI;

	[SerializeField] protected float outerAngleOffset;
	[SerializeField] protected float innerCurledUpRotation;
	[SerializeField] protected float outerCurledUpRotation;

	protected bool isLeftLeg;
	protected float climbingAngle;
	protected float innerAngleOffset;
	protected float climbCenterX;
	protected float climbRadius;
	protected float innerLength;
	protected float outerLength = 0.24f;
	protected LegState state = LegState.Climbing;
	protected bool transitioning = false;
	protected float lastInnerClimbingRotation;
	protected float lastOuterClimbingRotation;


	//------------------------------------------------------------------------------------------------------
	//
	//  Properties
	//
	//------------------------------------------------------------------------------------------------------

	public LegState State {
		get {
			return this.state;
		}
		set {
			if (this.state != value) {
				this.state = value;
				if (this.transitioning) {
					Tweener.StopAllTweeners(this.Inner);
					Tweener.StopAllTweeners(this.Outer);
				}
				switch (this.state) {
					case LegState.Climbing:
						this.transitionTo(this.lastInnerClimbingRotation, this.lastOuterClimbingRotation);
						break;

					case LegState.CurledUp:
						this.lastInnerClimbingRotation = this.Inner.localEulerAngles.z;
						this.lastOuterClimbingRotation = this.Outer.localEulerAngles.z;
						this.transitionTo(this.innerCurledUpRotation, this.outerCurledUpRotation);
						break;
				}
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void Initialize(bool isLeftLeg) {
		this.isLeftLeg = isLeftLeg;
		Vector3 innerVector = this.Outer.position - this.Inner.position;
		this.innerLength = innerVector.magnitude;
		this.climbCenterX = -this.Inner.transform.localPosition.x;
		this.innerAngleOffset = Mathf.Atan2(innerVector.y, innerVector.x);
	}

	public void SetClimbAngle(float angle) {

		this.climbingAngle = angle;
		if (this.state == LegState.Climbing && !this.transitioning) {
			this.updateClimbingState();
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void updateClimbingState() {
		float factor = this.isLeftLeg ? -1.0f : 1.0f;

		float tipX;
		float tipY;
		if (this.climbingAngle < Mathf.PI) {
			tipX = this.climbCenterX;
			tipY = ClimbCenterY + ClimbRadius - (this.climbingAngle / Mathf.PI) * ClimbRadius * 2.0f;
		} else {
			tipX = this.climbCenterX - factor * Mathf.Sin(this.climbingAngle) * ClimbRadius;
			tipY = ClimbCenterY + Mathf.Cos(this.climbingAngle) * ClimbRadius;
		}

		// Calculate distance from desired leg tip position to leg anchorpoint
		float straightLineAngle = Mathf.Atan2(tipY, tipX);
		float straightLineLength = Mathf.Sqrt(tipX * tipX + tipY * tipY);

		/* When the leg is in the desired position, it creates a triangle with this
		distance line. We now calculate the angle between this distance line and the
		first segment of the leg, using the law of cosines:

		(a, b and c are sides, and C is the angle opposite side c)

		c ^ 2 = a ^ 2 + b ^ 2 - 2 * a * b * cos(C)


		This gives us that:

		cos(C) = (a ^ 2 + b ^ 2 - c ^ 2) / (2 * a * b)

		*/

		// So we do this using ttl2 as side c:
		float segment1StraightLineAngle = Mathf.Acos((this.innerLength * this.innerLength + straightLineLength * straightLineLength - this.outerLength * this.outerLength) / (2.0f * this.innerLength * straightLineLength));
		float innerAngle = straightLineAngle - factor * segment1StraightLineAngle;
		float angle2 = factor * (Mathf.PI - Mathf.Acos((this.innerLength * this.innerLength + this.outerLength * this.outerLength - straightLineLength * straightLineLength) / (2.0f * this.innerLength * this.outerLength)));

		// Correct for angle offsets
		innerAngle -= this.innerAngleOffset;
		angle2 = angle2 + this.innerAngleOffset - this.outerAngleOffset;

		Vector3 euler = this.Inner.transform.localEulerAngles;
		euler.z = innerAngle * Mathf.Rad2Deg;
		this.Inner.transform.localEulerAngles = euler;
		euler.z = angle2 * Mathf.Rad2Deg;
		this.Outer.transform.localEulerAngles = euler;
	}

	protected void transitionTo(float innerAngle, float outerAngle) {
		Tweener inner = new Tweener(this.Inner, SpiderLegs.CurlAnimationDuration);
		inner.EasingFunction = TweenerEasingFunctions.QuadraticEaseInOut;
		inner.SetEuler(null, new Vector3(0.0f, 0.0f, innerAngle), true, true);
		Tweener outer = new Tweener(this.Outer, SpiderLegs.CurlAnimationDuration);
		outer.EasingFunction = TweenerEasingFunctions.QuadraticEaseInOut;
		outer.SetEuler(null, new Vector3(0.0f, 0.0f, outerAngle), true, true);
		outer.SetCompletionHandler(this.onTransitionComplete);
		inner.Start();
		outer.Start();
		this.transitioning = true;
	}

	protected void onTransitionComplete() {
		Tweener.StopAllTweeners(this.Inner);
		this.transitioning = false;
	}
}
