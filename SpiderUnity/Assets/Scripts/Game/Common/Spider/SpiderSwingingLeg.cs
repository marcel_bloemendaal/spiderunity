﻿using UnityEngine;
using System;
using UnityEngine.Serialization;

[Serializable]
public class SpiderSwingingLeg {

	public enum LegState {
		Swinging,
		CurledUp,
	}

	public Transform Inner;
	public Transform Outer;

	[SerializeField] protected float innerMaxSwingRotation;
	[SerializeField] protected float outerMaxSwingRotation;
	[SerializeField] protected float innerCurledUpRotation;
	[SerializeField] protected float outerCurledUpRotation;

	protected float innerBaseRotation;
	protected float outerBaseRotation;

	protected LegState state = LegState.Swinging;
	protected bool transitioning = false;
	protected float swingFactor;


	//------------------------------------------------------------------------------------------------------
	//
	//  Properties
	//
	//------------------------------------------------------------------------------------------------------

	public LegState State {
		get {
			return this.state;
		}
		set {
			if (this.state != value) {
				this.state = value;
				if (this.transitioning) {
					Tweener.StopAllTweeners(this.Inner);
					Tweener.StopAllTweeners(this.Outer);
				}
				switch (this.state) {
					case LegState.Swinging:
						this.transitionTo(this.innerBaseRotation, this.outerBaseRotation);
						break;

					case LegState.CurledUp:
						this.transitionTo(this.innerCurledUpRotation, this.outerCurledUpRotation);
						break;
				}
			}
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void Initialize() {
		this.innerBaseRotation = this.Inner.localEulerAngles.z;
		this.outerBaseRotation = this.Outer.localEulerAngles.z;
		this.innerCurledUpRotation = (this.innerCurledUpRotation + 360.0f) % 360.0f;
		this.outerCurledUpRotation = (this.outerCurledUpRotation + 360.0f) % 360.0f;
	}

	public void SetSwingFactor(float factor) {
		this.swingFactor = factor;
		if (this.State == LegState.Swinging && !this.transitioning) {
			this.updateSwingingState();
		}
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	protected void updateSwingingState() {
		Vector3 euler = this.Inner.localEulerAngles;
		euler.z = this.innerBaseRotation + this.swingFactor * innerMaxSwingRotation;
		this.Inner.localEulerAngles = euler;
		euler = this.Outer.localEulerAngles;
		euler.z = this.outerBaseRotation + this.swingFactor * outerMaxSwingRotation;
		this.Outer.localEulerAngles = euler;
	}

	protected void transitionTo(float innerAngle, float outerAngle) {
		Tweener inner = new Tweener(this.Inner, SpiderLegs.CurlAnimationDuration);
		inner.EasingFunction = TweenerEasingFunctions.QuadraticEaseInOut;
		inner.SetEuler(null, new Vector3(0.0f, 0.0f, innerAngle), true, true);
		Tweener outer = new Tweener(this.Outer, SpiderLegs.CurlAnimationDuration);
		outer.EasingFunction = TweenerEasingFunctions.QuadraticEaseInOut;
		outer.SetEuler(null, new Vector3(0.0f, 0.0f, outerAngle), true, true);
		outer.SetCompletionHandler(this.onTransitionComplete);
		inner.Start();
		outer.Start();
		this.transitioning = true;
	}

	protected void onTransitionComplete() {
		Tweener.StopAllTweeners(this.Inner);
		this.transitioning = false;
	}
}
