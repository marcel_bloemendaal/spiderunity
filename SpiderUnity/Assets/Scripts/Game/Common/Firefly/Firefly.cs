﻿using UnityEngine;

[RequireComponent(typeof(SpriteSwapLoop))]
public class Firefly : MonoBehaviour, ICheckpointEntity, IThreadResponder {
	[SerializeField] protected Sprite[] offSprites;
	[SerializeField] protected Sprite[] onSprites;

	protected SpriteSwapLoop spriteSwapLoop;
	protected bool on = false;
	protected bool wasOnAtSavedCheckPoint = false;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public void ResetToOriginalState() {
		this.setOn(false);
		this.wasOnAtSavedCheckPoint = false;
	}

	public void SaveCheckpoint(Firefly firefly) {
		this.wasOnAtSavedCheckPoint = this.on;
	}

	public void ResetToCheckpoint() {
		this.setOn(this.wasOnAtSavedCheckPoint);
	}

	//--------------------------------------------------
	//  IThreadResponder
	//--------------------------------------------------

	public ThreadResponse RespondToThread() {
		if (!this.on) {
			this.setOn(true);
			Environment.Current.SaveCheckpoint(this);
		}
		return ThreadResponse.Stick;
	}


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.spriteSwapLoop = this.GetComponent<SpriteSwapLoop>();
	}

	protected void Start() {
		Environment.Current.RegisterCheckpointEntity(this);
	}

	protected void OnDestroy() {
		Environment.Current.UnregisterCheckpointEntity(this);
	}

	//--------------------------------------------------
	//  Other
	//--------------------------------------------------

	protected void setOn(bool on) {
		this.on = on;
		this.spriteSwapLoop.Sprites = on ? this.onSprites : this.offSprites;
	}
}
