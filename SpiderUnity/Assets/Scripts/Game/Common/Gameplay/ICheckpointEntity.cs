﻿public interface ICheckpointEntity {
	void ResetToOriginalState();
	void SaveCheckpoint(Firefly firefly);
	void ResetToCheckpoint();
}
