﻿using UnityEngine;
using System.Collections.Generic;

public class ContactDamageDealer : MonoBehaviour {

	public int Damage = 1;

	protected List<IVulnerable> vulnerablesInContact = new List<IVulnerable>();


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//---------------------------------------------------
	//  Unity messages
	//---------------------------------------------------

	protected void FixedUpdate() {
		// Iterate backwards --> damage can cause things to die and thus be removed from the collection!
		for (int index = this.vulnerablesInContact.Count - 1; index >= 0; --index) {
			this.vulnerablesInContact[index].ReceiveDamage(Damage);
		}
	}

	protected void OnCollisionEnter2D(Collision2D collision) {
		IVulnerable vulnerable = collision.gameObject.GetComponent<IVulnerable>();
		if (vulnerable != null) {
			this.vulnerablesInContact.Add(vulnerable);
			vulnerable.ReceiveDamage(Damage);
		}
	}

	protected void OnCollisionExit2D(Collision2D collision) {
		IVulnerable vulnerable = collision.gameObject.GetComponent<IVulnerable>();
		if (vulnerable != null) {
			this.vulnerablesInContact.Remove(vulnerable);
		}
	}
}
