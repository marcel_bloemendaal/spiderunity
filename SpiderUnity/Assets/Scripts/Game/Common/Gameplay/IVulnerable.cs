﻿using System;
public interface IVulnerable {
	int Health { get; }
	void ReceiveDamage(int amount);
	void ReceiveLethalDamage();
}
