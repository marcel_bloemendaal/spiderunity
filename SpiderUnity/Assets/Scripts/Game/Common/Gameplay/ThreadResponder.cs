﻿using UnityEngine;
using System.Collections;

public class ThreadResponder : MonoBehaviour, IThreadResponder {

	[SerializeField]
	protected ThreadResponse threadResponse;


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public ThreadResponse RespondToThread() {
		return this.threadResponse;
	}
}
