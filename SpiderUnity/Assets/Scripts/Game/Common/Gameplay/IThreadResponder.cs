﻿public interface IThreadResponder {
	ThreadResponse RespondToThread();
}
