﻿using UnityEngine;

public class GameObjectUtils {


	//------------------------------------------------------------------------------------------------------
	//
	//  Public methods
	//
	//------------------------------------------------------------------------------------------------------

	public static void SetColorRecursively(GameObject gameObject, Color color) {
		foreach (SpriteRenderer sprite in gameObject.GetComponentsInChildren<SpriteRenderer>()) {
			sprite.color = color;
		}
	}
}
