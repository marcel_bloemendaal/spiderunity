﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSText : MonoBehaviour {

	protected Text text;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.text = this.GetComponent<Text>();
	}

	protected void Update() {
		this.text.text = string.Format("fps: {0:0.##}", 1.0f / Time.deltaTime);
	}
}
