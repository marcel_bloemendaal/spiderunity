﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class LoadSceneButtonAction : MonoBehaviour {

	[SerializeField] protected string sceneName;


	//------------------------------------------------------------------------------------------------------
	//
	//  Non-public methods
	//
	//------------------------------------------------------------------------------------------------------

	//--------------------------------------------------
	//  Unity messages
	//--------------------------------------------------

	protected void Awake() {
		this.GetComponent<Button>().onClick.AddListener(this.onButtonClick);
	}

	protected void onButtonClick() {
		if (this.sceneName != null) {
			SceneManager.LoadScene(this.sceneName);
		}
	}
}
