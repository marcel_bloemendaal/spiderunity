﻿using UnityEngine;
using System.Collections.Generic;


public class MathUtils {
	public static int Cycle(int value, int increment, int cycleSize) {
		int result = (value + increment) % cycleSize;
		return result < 0 ? (result + cycleSize) : result;
	}

	public static int[] CalculatePolygonTriangles(List<Vector2> vertices) {

		List<int> indices = new List<int>();
		for (int vertexIndex = 0; vertexIndex < vertices.Count; ++vertexIndex) {
			indices.Add(vertexIndex);
		}

		// We use the 'ear clipping algorithm

		// First we pass over all vertices to determine if they are the mid-point of an ear
		List<bool> ears = new List<bool>();
		for (int earIndex = 0; earIndex < indices.Count; ++earIndex) {
			ears.Add(IsVertexAnEar(earIndex, indices, vertices));
		}

		// Now we iterate over the vertices again, actually clipping the ears
		List<int> triangles = new List<int>();
		while(indices.Count > 2 && ears.Contains(true)) {
			int index = ears.IndexOf(true);
			if (ears[index]) {
				int v0Index = indices[Cycle(index, -1, indices.Count)];
				int v1Index = indices[index];
				int v2Index = indices[Cycle(index, 1, indices.Count)];
				triangles.Add(v0Index);
				triangles.Add(v1Index);
				triangles.Add(v2Index);
				indices.RemoveAt(index);
				ears.RemoveAt(index);
				int previousIndex = Cycle(index, -1, indices.Count);
				int safeIndex = Cycle(index, 0, indices.Count); // In case index was the last element
				if (indices.Count > 2) {
					ears[previousIndex] = IsVertexAnEar(previousIndex, indices, vertices);
					ears[Cycle(index, 0, indices.Count)] = IsVertexAnEar(safeIndex, indices, vertices);
				}
			}
		}

		return triangles.ToArray();
	}

	protected static bool IsVertexAnEar(int index, List<int> remainingIndices, List<Vector2> originalVertices) {
		int numIndices = remainingIndices.Count;
		bool result;
		Vector2 v0 = originalVertices[remainingIndices[Cycle(index, -1, numIndices)]];
		Vector2 v1 = originalVertices[remainingIndices[index]];
		Vector2 v2 = originalVertices[remainingIndices[Cycle(index, 1, numIndices)]];
		if (Vector2.Dot((v1 - v0).rightHandVector(), v2 - v1) < 0) {
			result = false;
		} else {
			int endIndex = Cycle(index, -1, numIndices);
			bool insidePointFound = false;
			for (int otherIndex = Cycle(index, 2, numIndices); otherIndex != endIndex; otherIndex = Cycle(otherIndex, 1, numIndices)) {
				if (IsPointInTriangle(originalVertices[remainingIndices[otherIndex]], v0, v1, v2)) {
					insidePointFound = true;
					break;
				}
			}
			result = !insidePointFound;
		}

		return result;
	}

	protected static float sign(Vector2 point, Vector2 v0, Vector2 v1) {
		return (point.x - v1.x) * (v0.y - v1.y) - (v0.x - v1.x) * (point.y - v1.y);
	}

	protected static bool IsPointInTriangle(Vector2 point, Vector2 v0, Vector2 v1, Vector2 v2) {
		float d0, d1, d2;
		bool hasNeg, hasPos;

		d0 = sign(point, v0, v1);
		d1 = sign(point, v1, v2);
		d2 = sign(point, v2, v0);

		hasNeg = (d0 < 0) || (d1 < 0) || (d2 < 0);
		hasPos = (d0 > 0) || (d1 > 0) || (d2 > 0);

		return !(hasNeg && hasPos);
	}
}

public static class Vector2Extensions {

	public static Vector2 leftHandVector(this Vector2 vector) {
		return new Vector2(-vector.y, vector.x);
	}

	public static Vector2 rightHandVector(this Vector2 vector) {
		return new Vector2(vector.y, -vector.x);
	}
}


