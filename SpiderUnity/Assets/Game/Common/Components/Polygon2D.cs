﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


[RequireComponent(typeof(MeshFilter)), RequireComponent(typeof(MeshRenderer)), ExecuteAlways]
public class Polygon2D : MonoBehaviour {

	/* Be careful! This monobehaviour has the attribute [ExecuteAlways] so all messages are also
	 * called in edit mode! This is done so the mesh can be updated after loading. Each Polygon2D
	 * (re)creates its own mesh. This is because we have to use the 'sharedMesh' property of the
	 * MeshFilter component. Using the 'mesh' property causes Unity to retain the mesh as an asset,
	 * and reuse it for copies of this GameObject as well. This causes the problem that when
	 * editing the polygon, all copies are edited as well. To prevent this, we use the sharedMesh
	 * property and recreate the mesh at start ourselves. To make sure this also happens in edit
	 * mode, this script has the [ExecuteAlways] attribute.
	 */

	protected const float TexturePixelsPerUnit = 250.0f;

	public List<Vector2> Vertices = new List<Vector2>();

	protected MeshFilter meshFilter;

	public void Reset() {
		if (this.Vertices.Count == 0) {
			this.Vertices.Add(Vector2.up);
			this.Vertices.Add(Vector2.down + Vector2.right);
			this.Vertices.Add(Vector2.down + Vector2.left);
		}
	}

	public void Start() {
		this.setup();
	}

	protected void setup() {
		this.meshFilter = this.GetComponent<MeshFilter>();
		this.meshFilter.sharedMesh = new Mesh();
		this.UpdateMesh();
	}

	public void UpdateMesh() {
		Mesh mesh = this.meshFilter.sharedMesh;
		if (mesh == null) {
			mesh = new Mesh();
			this.meshFilter.sharedMesh = mesh;
		}
		mesh.Clear();
		Vector3[] verticesArray = this.Vertices.Select(vertex => (Vector3)vertex).ToArray();
		mesh.vertices = verticesArray;
		mesh.triangles = MathUtils.CalculatePolygonTriangles(this.Vertices);
		mesh.uv = this.generateUVMap(verticesArray);
		mesh.RecalculateNormals();
	}

	protected Vector2[] generateUVMap(Vector3[] vertices) {
		float minX = float.MaxValue;
		float maxX = float.MinValue;
		float minY = float.MaxValue;
		float maxY = float.MinValue;
		foreach (Vector3 vertex in vertices) {
			if (vertex.x < minX) {
				minX = vertex.x;
			}
			if (vertex.x > maxX) {
				maxX = vertex.x;
			}
			if (vertex.y < minY) {
				minY = vertex.y;
			}
			if (vertex.y > maxY) {
				maxY = vertex.y;
			}
		}

		Material material = this.GetComponent<MeshRenderer>().sharedMaterial;
		Vector2 tileSize;
		if (material != null && material.mainTexture != null) {
			tileSize = new Vector2(material.mainTexture.width / TexturePixelsPerUnit, material.mainTexture.height / TexturePixelsPerUnit);
		} else {
			tileSize = Vector2.one;
		}

		List<Vector2> uvMap = new List<Vector2>();
		float xRange = (maxX - minX);
		float yRange = (maxY - minY);
		float xTiles = xRange / tileSize.x;
		float yTiles = yRange / tileSize.y;
		foreach (Vector3 vertex in vertices) {
			uvMap.Add(new Vector2((vertex.x - minX) / xRange * xTiles, (vertex.y - minY) / yRange * yTiles));
		}
		return uvMap.ToArray();
	}
}
