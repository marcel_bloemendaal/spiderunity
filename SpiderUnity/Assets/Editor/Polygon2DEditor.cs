﻿using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(Polygon2D)), CanEditMultipleObjects]
public class Polygon2DEditor : Editor {

	protected virtual void OnSceneGUI() {
        Polygon2D polygon2D = (Polygon2D)target;
        EditorGUI.BeginChangeCheck();
        Vector2 polygonPosition = polygon2D.transform.position;
        int selectedIndex = -1;
        for (int index = 0; index < polygon2D.Vertices.Count; ++index) {
            //this.vertices[index] = Handles.PositionHandle(polygon2D.transform.position + (Vector3)polygon2D.Vertices[index], Quaternion.identity) - polygon2D.transform.position;
            Vector2 vertex = polygonPosition + polygon2D.Vertices[index];
            HandlesExtended.Free2DMoveHandle(ref vertex, 0.1f, Quaternion.identity, Color.cyan, Color.green);
            if (HandlesExtended.free2DMoveHandle.selected) {
                selectedIndex = index;
			}
            polygon2D.Vertices[index] = vertex - polygonPosition;
        }
        GUILayout.Window(0, new Rect(10, 40, 200, 70), (id) => {
            if (GUILayout.Button("Add vertex")) {
                this.addVertex(polygon2D, selectedIndex);
                polygon2D.UpdateMesh();
            }
            GUI.enabled = selectedIndex != -1;
            if (GUILayout.Button("Remove vertex")) {
                polygon2D.Vertices.RemoveAt(selectedIndex);
                polygon2D.UpdateMesh();
            }
            GUI.enabled = true;
        }, string.Format("Polygon: {0}", polygon2D.gameObject.name));
        if (EditorGUI.EndChangeCheck()) {
            polygon2D.UpdateMesh();
		}
    }

    protected void addVertex(Polygon2D polygon2D, int selectedIndex) {
        switch(polygon2D.Vertices.Count) {
            case 0:
                polygon2D.Vertices.Add(Vector2.right);
                break;

            case 1:
                polygon2D.Vertices.Add(polygon2D.Vertices[0] + Vector2.right);
                break;

            default: {
                    int index = selectedIndex == -1 ? (polygon2D.Vertices.Count - 1) : selectedIndex;
                    int nextIndex = MathUtils.Cycle(index, 1, polygon2D.Vertices.Count);
                    Vector2 vertex = polygon2D.Vertices[index];
                    Vector2 nextVertex = polygon2D.Vertices[nextIndex];
                    Vector2 newVertex = vertex + 0.5f * (nextVertex - vertex);
                    polygon2D.Vertices.Insert(nextIndex, newVertex);
                    break;
				}
        }
    }
}