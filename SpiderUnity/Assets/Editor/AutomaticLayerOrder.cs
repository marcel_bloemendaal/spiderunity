﻿using UnityEditor;
using UnityEngine;

public class AutomaticLayerOrder
{
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("Level/Auto order in layer")]
    public static void AutoOrderInLayer()
    {
		AutomaticLayerOrderWindow automaticLayerOrderWindow = ScriptableObject.CreateInstance<AutomaticLayerOrderWindow>();
        automaticLayerOrderWindow.ShowModalUtility();
    }
}
