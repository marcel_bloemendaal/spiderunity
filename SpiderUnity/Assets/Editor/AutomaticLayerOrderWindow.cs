﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


public class AutomaticLayerOrderWindow : EditorWindow {

	public enum SortingRule {
		RightOverLeft,
		LeftOverRight,
		BottomOverTop,
		TopOverBottom
	}

	protected int startOrder = 0;
	protected float equalityOverlapPercentage = 50.0f;
	protected SortingRule primarySortingRule = SortingRule.TopOverBottom;
	protected SortingRule secondarySortingRule = SortingRule.LeftOverRight;


    //------------------------------------------------------------------------------------------------------
    //
    //  Non-public methods
    //
    //------------------------------------------------------------------------------------------------------

    //--------------------------------------------------
    //  Unity messages
    //--------------------------------------------------
    
    protected void OnGUI() {
        this.titleContent = new GUIContent("Options");
        this.startOrder = EditorGUILayout.IntField("Start order:", this.startOrder);
		this.equalityOverlapPercentage = EditorGUILayout.FloatField("Same row / column overlap percentage:", this.equalityOverlapPercentage);
		this.primarySortingRule = (SortingRule)EditorGUILayout.EnumPopup("Primary sorting rule:", this.primarySortingRule);
		this.secondarySortingRule = (SortingRule)EditorGUILayout.EnumPopup("Secondary sorting rule:", this.secondarySortingRule);

		if (GUILayout.Button("OK")) {
			this.applyLayerOrder();
			this.Close();
        }
        
        if (GUILayout.Button("Cancel")) {
            this.Close();
        }
    }

	protected void applyLayerOrder() {
		List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
		GameObject container = GameObject.Find("Rocks");
		if (container != null) {
			//foreach (GameObject gameObject in Selection.gameObjects) {
			foreach (Transform child in container.transform) {
				SpriteRenderer spriteRenderer = child.gameObject.GetComponent<SpriteRenderer>();
				if (spriteRenderer != null) {
					spriteRenderers.Add(spriteRenderer);
				}
			}
		}
		spriteRenderers.Sort((SpriteRenderer left, SpriteRenderer right) => {
			bool verticalFirst = this.primarySortingRule == SortingRule.BottomOverTop || this.primarySortingRule == SortingRule.TopOverBottom;
			int result;
			if (verticalFirst) {
				result = this.compareSpritePositionVertically(left, right, this.primarySortingRule == SortingRule.TopOverBottom);
			} else {
				result = this.compareSpritePositionHorizontally(left, right, this.primarySortingRule == SortingRule.RightOverLeft);
			}
			if (result == 0) {
				switch(this.secondarySortingRule) {
					case SortingRule.BottomOverTop:
						result = (left.transform.position.y < right.transform.position.y) ? 1 : -1;
						break;

					case SortingRule.LeftOverRight:
						result = (left.transform.position.x < right.transform.position.x) ? 1 : -1;
						break;

					case SortingRule.RightOverLeft:
						result = (left.transform.position.x > right.transform.position.x) ? 1 : -1;
						break;

					case SortingRule.TopOverBottom:
						result = (left.transform.position.y > right.transform.position.y) ? 1 : -1;
						break;
				}
			}
			return result;
		});
		int order = startOrder;
		foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
			spriteRenderer.sortingOrder = order++;
		}
	}

	protected int compareSpritePositionHorizontally(SpriteRenderer left, SpriteRenderer right, bool higherValueOnTop) {
		float leftWidth = left.sprite.bounds.size.x;
		float rightWidth = right.sprite.bounds.size.x;
		float dx = left.transform.position.x - right.transform.position.x;
		int result = 0;
		float equalityOverlapFactor = this.equalityOverlapPercentage / 100.0f;
		float maxEqualityDistance = (leftWidth + rightWidth) * 0.5f - (leftWidth < rightWidth ? (leftWidth * equalityOverlapFactor) : (rightWidth * equalityOverlapFactor));
		if (dx > maxEqualityDistance) {
			result = 1;
		}
		else if (dx < -maxEqualityDistance) {
			result = -1;
		}

		Debug.Log(string.Format("dx: {0}, maxEqualityDistance: {1}, result: {2}, higherOnTop: {3}", dx, maxEqualityDistance, result, higherValueOnTop));
		return higherValueOnTop ? result : -result;
	}

	protected int compareSpritePositionVertically(SpriteRenderer left, SpriteRenderer right, bool higherValueOnTop) {
		float leftHeight = left.sprite.bounds.size.y;
		float rightHeight = right.sprite.bounds.size.y;
		float dy = left.transform.position.y - right.transform.position.y;
		int result = 0;
		float equalityOverlapFactor = this.equalityOverlapPercentage / 100.0f;
		float maxEqualityDistance = (leftHeight + rightHeight) * 0.5f - (leftHeight < rightHeight ? (leftHeight * equalityOverlapFactor) : (rightHeight * equalityOverlapFactor));
		if (dy > maxEqualityDistance) {
			result = 1;
		}
		else if (dy < -maxEqualityDistance) {
			result = -1;
		}

		return higherValueOnTop ? result : -result;
	}
}